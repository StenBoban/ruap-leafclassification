﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeafClasification
{
    public partial class Form1 : Form
    {

        public string resultedLeafPlaceholder = "Resulted leaf is ";
        public string percentagePlaceholder = "Percentage of accuracy = ";
        public string filePath = "";

        public Form1()
        {
            InitializeComponent();  
        }
        private void Btn_getImageFeaturesFromFile_Click(object sender, EventArgs e)
        {

            lb_resultedLeafText.Text = "";

            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "Text|*.txt";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = false;

            if (choofdlog.ShowDialog() == DialogResult.OK)
            {
                filePath = choofdlog.FileName;
                string[] arrAllFiles = choofdlog.FileNames; //used when Multiselect = true    

                btn_getImageFeaturesFromFile.Enabled = false;
                InvokeRequestResponseService();
            }


        }



        public async Task InvokeRequestResponseService()
        {
            using (var client = new System.Net.Http.HttpClient())
            {

                FileReader myReader = new FileReader(filePath);
                List<string> features = myReader.ReadFromFile();
               

                var scoreRequest = new
                {

                    Inputs = new Dictionary<string, StringTable>() {
                        {
                            "input1",
                            new StringTable()
                            {
                                ColumnNames = new string[] {"id", "area", "perimeter", "physiological_length", "physiological_width", "aspect_ratio", "rectangularity", "circularity", "mean_r", "mean_g", "mean_b", "stddev_r", "stddev_g", "stddev_b", "contrast", "correlation", "inverse_difference_moments", "entropy"},
                                Values = new string[,]  { {
                                        features[0],
                                        features[1],
                                        features[2],
                                        features[3],
                                        features[4],
                                        features[5],
                                        features[6],
                                        features[7],
                                        features[8],
                                        features[9],
                                        features[10],
                                        features[11],
                                        features[12],
                                        features[13],
                                        features[14],
                                        features[15],
                                        features[16],
                                        features[17]

                                    } }
                            }
                        },
                    },
                    GlobalParameters = new Dictionary<string, string>()
                    {
                    }
                };

                api leafApi = new api();
               
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", leafApi.GetKey());
                client.BaseAddress = new Uri(leafApi.GetUri());

                HttpResponseMessage response = await client.PostAsJsonAsync("", scoreRequest);

                if (response.IsSuccessStatusCode)
                {
                    btn_getImageFeaturesFromFile.Enabled = true;

                    string result = await response.Content.ReadAsStringAsync();

                    Parser leafParser = new Parser(result);
                    
                    lb_resultedLeafText.Text = resultedLeafPlaceholder + leafParser.GetLeaf();

                }
                else
                {
                    btn_getImageFeaturesFromFile.Enabled = true;

                    MessageBox.Show("Oops..dogodila se pogreška", "Error");

                    string responseContent = await response.Content.ReadAsStringAsync();
                }
            }
        }

        
    }


}
