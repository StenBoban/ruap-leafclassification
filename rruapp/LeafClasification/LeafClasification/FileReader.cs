﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeafClasification
{
    class FileReader
    {
        string filePath;

        public FileReader(string filePath)
        {
            this.filePath = filePath;
        }


        public List<string> ReadFromFile()
        {
            List<string> values = new List<string>();
            try
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        var lineValues = line.Split(',');
                        foreach (string lineVal in lineValues)
                        {
                            values.Add(lineVal);
                        }
                    }
                    return values;
                }
            }
            catch (Exception e)
            {
                throw new Exception();
            }
        }
    }
}
