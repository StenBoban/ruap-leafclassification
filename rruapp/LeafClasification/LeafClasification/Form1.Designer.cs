﻿namespace LeafClasification
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_getImageFeaturesFromFile = new System.Windows.Forms.Button();
            this.lb_resultedLeafText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_getImageFeaturesFromFile
            // 
            this.btn_getImageFeaturesFromFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(57)))), ((int)(((byte)(43)))));
            this.btn_getImageFeaturesFromFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_getImageFeaturesFromFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_getImageFeaturesFromFile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(195)))), ((int)(((byte)(199)))));
            this.btn_getImageFeaturesFromFile.Location = new System.Drawing.Point(82, 32);
            this.btn_getImageFeaturesFromFile.Name = "btn_getImageFeaturesFromFile";
            this.btn_getImageFeaturesFromFile.Size = new System.Drawing.Size(581, 64);
            this.btn_getImageFeaturesFromFile.TabIndex = 1;
            this.btn_getImageFeaturesFromFile.Text = "Get Image Features From File";
            this.btn_getImageFeaturesFromFile.UseVisualStyleBackColor = false;
            this.btn_getImageFeaturesFromFile.Click += new System.EventHandler(this.Btn_getImageFeaturesFromFile_Click);
            // 
            // lb_resultedLeafText
            // 
            this.lb_resultedLeafText.AutoSize = true;
            this.lb_resultedLeafText.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_resultedLeafText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(57)))), ((int)(((byte)(43)))));
            this.lb_resultedLeafText.Location = new System.Drawing.Point(77, 171);
            this.lb_resultedLeafText.Name = "lb_resultedLeafText";
            this.lb_resultedLeafText.Size = new System.Drawing.Size(0, 29);
            this.lb_resultedLeafText.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(195)))), ((int)(((byte)(199)))));
            this.ClientSize = new System.Drawing.Size(751, 225);
            this.Controls.Add(this.lb_resultedLeafText);
            this.Controls.Add(this.btn_getImageFeaturesFromFile);
            this.Name = "Form1";
            this.Text = "Leaf Clasification";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_getImageFeaturesFromFile;
        private System.Windows.Forms.Label lb_resultedLeafText;
    }
}

