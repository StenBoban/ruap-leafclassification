﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeafClasification
{
    class Parser
    {
        string response;

        public Parser(string response)
        {
            this.response = response;
        }

        public string GetLeaf()
        {
            int count = 0;
            StringBuilder leaf = new StringBuilder();
            for(int i = response.Length - 1; count != 2; i--)
            {
                if (response[i].Equals('"')) count++;
                if (count == 2)
                {
                    
                    for(int j = i + 1; !response[j].Equals('"'); j++)
                    {
                        leaf.Append(response[j]);
                    }
                    break;
                }
            }
            return leaf.ToString();
        }
    }
}
